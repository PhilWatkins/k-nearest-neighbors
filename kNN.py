
# imports for the implementation
import numpy as np
from numpy import linalg as LA  # used for calculating norms
from collections import Counter # used to count up votes

# imports for testing only
from sklearn.datasets import load_wine
from sklearn.model_selection import train_test_split
from sklearn import metrics

class kNN:
    def __init__(self, k=5, p=2):
        """
        k: The number of neighbors used in the classification
        p: Specifies the p-norm to use
        p=infinity should be an acceptable value
        """
        self.k = k # number of neighbors
        self.p = p # 1, 2 or np.inf

    def fit(self, X_train, y_train):
        """
        X_train: numpy array of features, one instance per row
        y_train: numpy array of labels
        """
        self.X_train = X_train
        self.y_train = y_train
        return self # allows for calls like classifier.fit(X_train, y_train).predict(X_test)

    def predict(self, X_test):
        """
        X_test: numpy array of features, one instance per row
        """
        distances = []
        classifications = [] # change to np array later

        for i in range(len(X_test)):
            for j in range(len(self.X_train)):
                # append distance, label pairs (as tuples)
                distances.append( ( LA.norm( (self.X_train[j] - X_test[i] ), self.p), self.y_train[j]) )

            # sort the distances so that retrieving the value at index 0 to k gives us the k nearest neighbors
            distances.sort() 
            distances = distances[:self.k]

            labels = np.array( [x[1] for x in distances] ) # make a numpy array of labels
            votes = np.bincount(labels) # vote on a label based on its frequency

            classifications.append(np.argmax(votes)) # classification complete, add it to the list

        return np.array(classifications)
              

if __name__ == "__main__":
    pass
    # Get data from an sklearn dataset for testing purposes

    # features, labels = load_wine(return_X_y=True)
    # X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.20, random_state=42)

    # classifier = kNN(k=9, p=2) # works for p=float('inf') too

    # y_pred = classifier.fit(X_train, y_train).predict(X_test)

    # print(metrics.accuracy_score(y_test, y_pred))
